#include <WiFi.h>


const char* ssid     = "ssid";
const char* password = "mdp";

#include <Servo.h>

#define LED 4

//
//static const int servopinA = 21;
//static const int servopinB = 22;
static const int servopinA = 2;
static const int servopinB = 14;
Servo servo1;
Servo servo2;

WiFiServer server(80);
IPAddress local_IP(192, 168, 43, 184);
IPAddress gateway(192, 168, 43, 1);
IPAddress subnet(255, 255, 0, 0);



void handle_servo(void){
  WiFiClient client = server.available();   
  String value = "";
  bool etat_led = true;
  if (client) {                             
    while (client.connected()) {            
      if (client.available()) {             
        String v = "motorX";
        String rvc = client.readStringUntil('\r');
        String motor = rvc.substring(0,v.length());
        String value = rvc.substring(v.length());
        int c = value.toInt();
        if((motor == "motor1"))
          servo1.write(c);
        else if((motor == "motor2"))
          servo2.write(c);
       
      }
    }
    client.stop();
 }
}

void setup(){     
  if (!WiFi.config(local_IP, gateway, subnet)) {
    Serial.println("STA Failed to configure");
  }
    servo1.attach(servopinA);
    servo2.attach(servopinB);
    Serial.begin(115200);
    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    server.begin();
}

void loop(){
	handle_servo(); 
}
