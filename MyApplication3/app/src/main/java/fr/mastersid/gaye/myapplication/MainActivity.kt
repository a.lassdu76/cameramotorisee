package fr.mastersid.gaye.myapplication

import android.os.Build
import android.os.Bundle
import android.webkit.WebViewClient
import android.widget.SeekBar
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.goodiebag.protractorview.ProtractorView
import com.goodiebag.protractorview.ProtractorView.OnProtractorViewChangeListener
import fr.mastersid.gaye.myapplication.databinding.ActivityMainBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import java.io.BufferedOutputStream
import java.io.IOException
import java.io.OutputStreamWriter
import java.io.PrintWriter
import java.net.HttpURLConnection
import java.net.Socket
import java.net.URL
import kotlin.properties.Delegates


class MainActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        val iPcAM: String = ("http://192.168.43.101:5000/")
        val iPsERVO: String = ("192.168.43.184")
        setContentView(binding.root)
        supportActionBar?.hide()
        binding.webview.webViewClient = WebViewClient()
        binding.webview.loadUrl(iPcAM)
        binding.seekBar.max = 50
        binding.seekBar2.max = 180
        binding.seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                val motorNumber = 1
                CoroutineScope(Default).launch {
                    main(i,iPsERVO,motorNumber)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
            }
        })
        binding.seekBar2.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                val motorNumber = 2
                CoroutineScope(Default).launch {
                    main(i,iPsERVO,motorNumber)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
            }
        })

    }
    private fun main(values: Int, IP: String, motorNumber: Int) {
        val client = Socket(IP, 80)
        val printwriter = PrintWriter(client.getOutputStream(), true)
        printwriter.write("motor$motorNumber$values")
        printwriter.close()
        printwriter.flush()
    }
}